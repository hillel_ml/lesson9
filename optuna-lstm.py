def run(model=None, data=None):
    global_start_time = time.time()

    print('Loading data... ')
    data_b = pd.read_csv(input_file,
                         parse_dates=[0],
                         infer_datetime_format=True)

    # Removing NaN values
    data_b = data_b.dropna()

    print(data_b.head())

    # Removing the columns
    # data_b = data_b.drop(columns=["Open",
    #                                 "High",
    #                                 "Low",
    #                                 "Adj Close",
    #                                 "Volume"
    # ])
    
    #data = data_b['cpu']
    data = data_b['Close'].to_numpy()
    # data = data_b['cpu'].to_numpy()

    # train on first 700 samples and test on next 300 samples (test set has anomaly)
    X_train, y_train, X_test, y_test = prepare_data(data, 0, 500, 300, 550)
    # X_train, y_train, X_test, y_test = train_test_split(data["Date"],
                                                    #     data["Close"],
                                                    #     test_size=0.30,
                                                    #     random_state=42
                                                    # )

    img_x, img_y = X_train.shape[1], X_train.shape[2]
    input_shape = (img_x, img_y, 1)

    # if model is None:
    #     model = generate_model()

    # try:
    #     print("Training...")
    #     model.fit(
    #             X_train,
    #             y_train,
    #             validation_data=(X_test, y_test),
    #             batch_size=batch_size,
    #             epochs=epochs,
    #             validation_split=0.05,
    #             verbose=False,
    #             shuffle=True,
    #             callbacks=[tensorboard_callback]
    #         )
        
    #     print("Predicting...")
    #     predicted = model.predict(X_test)

    #     print("Reshaping predicted")
    #     predicted = np.reshape(predicted, (predicted.size,))
    # except KeyboardInterrupt:

    #     print("prediction exception")
    #     print('Training duration:{}'.format(time.time() - global_start_time))
    #     return model, y_test, 0
    
    # # Evaluate the model accuracy on the validation set.
    # score = model.evaluate(X_test, y_test, verbose=0)
    # print(f'Test results - Loss: {score[0]} - Accuracy: {score[1]}%')

    # try:
    #     plt.figure(figsize=(20, 8))
    #     plt.plot(y_test[:len(y_test)], 'b', label='Observed')
    #     plt.plot(predicted[:len(y_test)], 'g', label='Predicted')
    #     plt.plot(((y_test - predicted) ** 2), 'r', label='Root-mean-square deviation')
    #     plt.legend()
    #     plt.show()
    # except Exception as e:
    #     print("plotting exception")
    #     print(str(e))
    # print('Training duration:{}'.format(time.time() - global_start_time))

    return model, y_test, predicted























def train_and_evaluate(param, model, trail):
    
    # Load Data
    train_dataloader = torch.utils.data.DataLoader(Train_Dataset, batch_size=batch_size)
    Test_dataloader = torch.utils.data.DataLoader(Test_Dataset, batch_size=batch_size)

    criterion = nn.MSELoss()
    optimizer = getattr(optim, param['optimizer'])(model.parameters(), lr= param['learning_rate'])
    acc = nn.L1Loss()

    # Training Loop
    for epoch_num in range(EPOCHS):

            # Trainings
            total_loss_train = 0
            for train_input, train_target in train_dataloader:

                output = model.forward(train_input.float())
                batch_loss = criterion(output, train_target.float())
                total_loss_train += batch_loss.item()

                model.zero_grad()
                batch_loss.backward()
                optimizer.step()
            
            # Evaluation
            total_loss_val = 0
            total_mae = 0
            with torch.no_grad():

                for test_input, test_target in Test_dataloader:

                    output = model(test_input.float())

                    batch_loss = criterion(output, test_target)
                    total_loss_val += batch_loss.item()
                    batch_mae = acc(output, test_target)
                    total_mae  += batch_mae.item()
            
            accuracy = total_mae/len(Test_Dataset)

            # Add prune mechanism
            trail.report(accuracy, epoch_num)

            if trail.should_prune():
                raise optuna.exceptions.TrialPruned()

    return accuracy






# def get_best_parameters(args, Dtr, Val):
#     def objective(trial):
#         model = TransformerModel(args).to(args.device)
#         loss_function = nn.MSELoss().to(args.device)
#         optimizer = trial.suggest_categorical('optimizer',
#                                               [torch.optim.SGD,
#                                                torch.optim.RMSprop,
#                                                torch.optim.Adam])(
#             model.parameters(), lr=trial.suggest_loguniform('lr', 5e-4, 1e-2))
#         print('training...')
#         epochs = 10
#         val_loss = 0
#         for epoch in range(epochs):
#             train_loss = []
#             for batch_idx, (seq, target) in enumerate(Dtr, 0):
#                 seq, target = seq.to(args.device), target.to(args.device)
#                 optimizer.zero_grad()
#                 y_pred = model(seq)
#                 loss = loss_function(y_pred, target)
#                 train_loss.append(loss.item())
#                 loss.backward()
#                 optimizer.step()
#             # validation
#             val_loss = get_val_loss(args, model, Val)

#             print('epoch {:03d} train_loss {:.8f} val_loss {:.8f}'.format(epoch, np.mean(train_loss), val_loss))
#             model.train()

#         return val_loss

#     sampler = optuna.samplers.TPESampler()
#     study = optuna.create_study(sampler=sampler, direction='minimize')
#     study.optimize(func=objective, n_trials=5)
#     pruned_trials = study.get_trials(deepcopy=False,
#                                      states=tuple([TrialState.PRUNED]))
#     complete_trials = study.get_trials(deepcopy=False,
#                                        states=tuple([TrialState.COMPLETE]))
#     best_trial = study.best_trial
#     print('val_loss = ', best_trial.value)
#     for key, value in best_trial.params.items():
#         print("{}: {}".format(key, value))