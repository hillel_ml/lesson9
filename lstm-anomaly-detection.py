import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
import time
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense, Activation, Dropout

epochs = 3 #3
batch_size = 50

# Каждая строка обучающего набора должна иметь длину 100-1,
# потому что последнее значение в каждой последовательности является меткой
sequence_length = 100 #100


def generate_model():

    model = Sequential()

    # Первый слой LSTM сети определяется длиной входящей последовательности
    model.add(LSTM(input_shape=(sequence_length-1, 1),
                   units=32,
                   return_sequences=True))
    model.add(Dropout(0.2))

    # Второй слой LSTM сети со 128 узлами
    model.add(LSTM(units=128,
                   return_sequences=True))
    model.add(Dropout(0.2))

    # Третий слой LSTM сети со 100 узлами
    model.add(LSTM(units=100,
                   return_sequences=False))
    model.add(Dropout(0.2))

    # Полносвязный выходной слой с линейной функцией активации
    model.add(Dense(units=1))
    model.add(Activation('linear'))

    # Компиляция модели. Функция активации - среднеквадратичная, алгоритм оптимизации - RMSProp
    model.compile(loss='mean_squared_error', optimizer='rmsprop')
    
    return model


def normalize(result):
    result_mean = result.mean()
    result_std = result.std()
    result -= result_mean
    result /= result_std
    return result, result_mean


def prepare_data(data, train_start, train_end, test_start, test_end):
    print("Размер выборки", len(data))

    # Создание обучающего набора
    print("Creating training data...")

    result = []
    for index in range(train_start, train_end - sequence_length):
        result.append(data[index: index + sequence_length])
    result = np.array(result)
    result, result_mean = normalize(result)

    print("Training data shape  : ", result.shape)

    train = result[train_start:train_end, :]
    np.random.shuffle(train)
    X_train = train[:, :-1]
    y_train = train[:, -1]

    # Создание тестового набора
    print("Creating test data...")

    result = []
    for index in range(test_start, test_end - sequence_length):
        result.append(data[index: index + sequence_length])
    result = np.array(result)
    result, result_mean = normalize(result)

    print("Test data shape  : {}".format(result.shape))

    X_test = result[:, :-1]
    y_test = result[:, -1]

    # Размерности обучающего и тестового наборов
    print("Shape X_train :", np.shape(X_train))
    print("Shape X_test  :", np.shape(X_test))

    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

    return X_train, y_train, X_test, y_test


def run(model=None, data=None):
    global_start_time = time.time()

    print('Loading data... ')
    data_b = pd.read_csv('lesson9/cpu-full-b.csv',
                         parse_dates=[0],
                         infer_datetime_format=True)

    #data = data_b['cpu']
    data = data_b['cpu'].to_numpy()

    # train on first 700 samples and test on next 300 samples (test set has anomaly)
    X_train, y_train, X_test, y_test = prepare_data(data, 0, 600, 400, 660)

    if model is None:
        model = generate_model()

    try:
        print("Training...")
        model.fit(
                X_train, y_train,
                batch_size=batch_size,
                epochs=epochs,
                validation_split=0.05)

        print("Predicting...")
        predicted = model.predict(X_test)

        print("Reshaping predicted")
        predicted = np.reshape(predicted, (predicted.size,))
    except KeyboardInterrupt:

        print("prediction exception")
        print('Training duration:{}'.format(time.time() - global_start_time))
        return model, y_test, 0

    try:
        plt.figure(figsize=(20, 8))
        plt.plot(y_test[:len(y_test)], 'b', label='Observed')
        plt.plot(predicted[:len(y_test)], 'g', label='Predicted')
        plt.plot(((y_test - predicted) ** 2), 'r', label='Root-mean-square deviation')
        plt.legend()
        plt.show()
    except Exception as e:
        print("plotting exception")
        print(str(e))
    print('Training duration:{}'.format(time.time() - global_start_time))

    return model, y_test, predicted



model, y_test, predicted = run()

